import { Component, ElementRef, ViewChild } from "@angular/core";
import { NavController, NavParams, AlertController } from "ionic-angular";
import { ParsersService } from "../../providers/aws.lambda-parsers";

/**
 * Generated class for the InvoicesCreatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-invoices-create',
  templateUrl: 'invoices-create.html',
})
export class InvoicesCreatePage {
  @ViewChild('fileInput') myFileInput: ElementRef;

  public isParsingProducts = false;
  public isParsingNfe = false;
  public isParsingProductsDone = false;
  public isParsingNfeDone = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public parsers: ParsersService,
    private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicesCreatePage');
  }

  parseProducts(event) {
    this.isParsingProducts = true;
    this.isParsingProductsDone = false;
    if (event.target.files !== undefined && event.target.files[0] !== undefined) {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        const htmlContent: string = e.target.result;
        const isValidProductsHtml = htmlContent.indexOf('CFOP') !== -1;
        if (isValidProductsHtml){
          this.parsers.parseProducts(e.target.result).subscribe(res => {
            console.log(res);
            this.isParsingProducts = false;
            this.isParsingProductsDone = true;
          });
        }else {
          this.wrongHTMLAlert('Este não é o HTML de Produtos e Serviços.');
          this.isParsingProducts = false;
        }
      }

      reader.readAsText(event.target.files[0]);
    }
  }

  parseNfce(event) {
    this.isParsingNfe = true;
    this.isParsingNfeDone = false;
    if (event.target.files !== undefined && event.target.files[0] !== undefined) {
      let reader = new FileReader();

      reader.onload = (e: any) => {
        const htmlContent: string = e.target.result;
        const validHtml = htmlContent.indexOf('Dados da NFC-e') !== -1;
        if (validHtml){
          this.parsers.parseNfce(e.target.result).subscribe(res => {
            console.log(res);
            this.isParsingNfe = false;
            this.isParsingNfeDone = true;
          });
        }else {
          this.wrongHTMLAlert('Este não é o HTML da aba Nfe.');
          this.isParsingNfe = false;
        }
      }

      reader.readAsText(event.target.files[0]);
    }
  }

  wrongHTMLAlert(subTitle) {
    let alert = this.alertCtrl.create({
      title: 'HTML incorreto',
      subTitle: subTitle,
      buttons: ['Ok']
    });
    alert.present();
  }
}
