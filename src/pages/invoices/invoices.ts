import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InvoicesCreatePage } from "../invoices-create/invoices-create";

/**
 * Generated class for the InvoicesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-invoices',
  templateUrl: 'invoices.html',
})
export class InvoicesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoicesPage');
  }

  addInvoice(){
    this.navCtrl.push(InvoicesCreatePage);
  }

}
