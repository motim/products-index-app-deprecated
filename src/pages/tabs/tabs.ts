import { Component } from '@angular/core';

import { SettingsPage } from '../settings/settings';
import { TasksPage } from '../tasks/tasks';
import { InvoicesPage } from "../invoices/invoices";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = InvoicesPage;
  tab2Root = TasksPage;
  tab3Root = SettingsPage;

  constructor() {

  }
}
