import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ParsersService {

  public devEndpoint = 'https://4o9rkgjlt9.execute-api.us-east-1.amazonaws.com/dev';
  public parseProductsEndpoint = this.devEndpoint + '/parse/products';
  public parseNfceEndpoint = this.devEndpoint + '/parse/nfce';

  constructor(private http: Http) {}

  parseProducts(html_string) {
    const applicationJson = new Headers({ 'Content-Type': 'application/json' });
    const header = {
      headers: new Headers({ 'Content-Type': 'application/json' })
    }
    let body = JSON.stringify({ 'html_string': html_string });
    return this.http.post(this.parseProductsEndpoint, body, header)
      .map((r: Response) => r.json());
  }

  parseNfce(html_string) {
    const applicationJson = new Headers({ 'Content-Type': 'application/json' });
    const header = {
      headers: new Headers({ 'Content-Type': 'application/json' })
    }
    let body = JSON.stringify({ 'html_string': html_string });
    return this.http.post(this.parseNfceEndpoint, body, header)
      .map((r: Response) => r.json());
  }

}
