import { Cognito } from './aws.cognito';
import { DynamoDB } from './aws.dynamodb';
import { User } from './user';
import { ParsersService } from "./aws.lambda-parsers";

export {
  Cognito,
  DynamoDB,
  User,
  ParsersService
};
